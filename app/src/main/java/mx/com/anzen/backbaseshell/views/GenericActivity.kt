package mx.com.anzen.backbaseshell.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mx.com.anzen.backbaseshell.presentators.GenericPresentator

abstract  class GenericActivity : AppCompatActivity() {
    protected  var presentator: GenericPresentator? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presentator?.onCreate()
    }

    override fun onResume() {
        super.onResume()
        presentator?.onResume()
    }

    override fun onPause() {
        super.onPause()
        presentator?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        presentator?.onDestroy()
    }

}
