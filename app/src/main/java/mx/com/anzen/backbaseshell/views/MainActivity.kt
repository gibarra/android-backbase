package mx.com.anzen.backbaseshell.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import mx.com.anzen.backbaseshell.R
import mx.com.anzen.backbaseshell.views.GenericActivity

/**
 * Created by anzendigital on 5/28/18.
 */
class MainActivity:GenericActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    companion object {


        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            return intent
        }
    }
}