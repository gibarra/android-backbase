package mx.com.anzen.backbaseshell.presentators

import android.content.Intent
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.backbase.android.model.ModelSource
import mx.com.anzen.backbaseshell.BackbaseApplication
import mx.com.anzen.backbaseshell.views.MainActivity

/**
 * Created by anzendigital on 5/28/18.
 */

class SplashPresentator(private val activity: AppCompatActivity) : GenericPresentator() {

    override fun onCreate() {

    }

    override fun onResume() {

        val app = activity.applicationContext as BackbaseApplication

        app.mannager?.loadModel(ModelSource.SERVER){
            activity.startActivity(MainActivity.newIntent(activity))
            activity.finish()
        }


    }

    override fun onPause() {

    }

    override fun onDestroy() {

    }
}
