package mx.com.anzen.backbaseshell.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mx.com.anzen.backbaseshell.R
import mx.com.anzen.backbaseshell.presentators.SplashPresentator

class SplashActivity : GenericActivity(){
    init {
        presentator =  SplashPresentator(this as AppCompatActivity);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}
