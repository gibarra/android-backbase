package mx.com.anzen.backbaseshell.backbase_utils

import android.content.Context
import android.util.Log
import com.backbase.android.Backbase
import com.backbase.android.listeners.ModelListener
import com.backbase.android.model.Model
import com.backbase.android.model.ModelSource
import mx.com.anzen.backbaseshell.BackbaseApplication

/**
 * Created by anzendigital on 5/28/18.
 */
class BackbaseMannager(backbaseApplication: BackbaseApplication) {
    val CONFIG_PATH = "backbase/conf/config.json"
    private var context:Context

    init{
        context = backbaseApplication
    }

    fun initBackbase(){
        Backbase.setLogLevel(Backbase.LogLevel.DEBUG)
        Backbase.initialize(context, CONFIG_PATH, false)
    }

    fun loadModel(source: ModelSource = ModelSource.SERVER,onLoaded:()->(Unit) ) {
        Backbase.getInstance().getModel(object:ModelListener<Model>{
            override fun onModelReady(model: Model?) {
                onLoaded()
            }
            override fun onError(error: String?) {
                Log.d("Backbase Model",error)
            }
        },source)
    }


}
