package mx.com.anzen.backbaseshell.presentators

/**
 * Created by anzendigital on 5/28/18.
 */

abstract class GenericPresentator {
    abstract fun onCreate()
    abstract fun onResume()
    abstract fun onPause()
    abstract fun onDestroy()
}
