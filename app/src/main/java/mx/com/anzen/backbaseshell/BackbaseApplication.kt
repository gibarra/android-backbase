package mx.com.anzen.backbaseshell

import android.app.Application
import com.backbase.android.Backbase

import mx.com.anzen.backbaseshell.backbase_utils.BackbaseMannager

/**
 * Created by anzendigital on 5/28/18.
 */

class BackbaseApplication : Application() {
    var mannager:BackbaseMannager? = null
    private set

    override fun onCreate() {
        super.onCreate()
        Backbase.setLogLevel(Backbase.LogLevel.DEBUG)
        mannager = BackbaseMannager(this)
        mannager?.initBackbase()
    }
}
